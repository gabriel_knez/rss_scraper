README

GENERAL NOTES:

    This application provides REST API, to manage celery task, fetch
    exchange rates from https://www.ecb.europa.eu/home/html/rss.en.html and return data.
    Default settings don`t provide any feed source, so in order to fetch data, first use /api/get_rates/ endpoint (usage down below).
    By default scheduler is set to fire every 5 seconds (only for test purposes, generally it would be set to run once a day)

    I was given https://www.ecb.europa.eu/home/html/rss.en.html link, as rss source, so my architecture assumes (and checks),
    that all url paths provided in /api/add_source/ are from that site. This assumption resulted in another one, also reflected
    in database architecture - base currency for all exchange rates is set as "EUR".

    IMPORTANT:
            If one does not want to programmatically use provided API, instead just test it, there is visual representation
            of endpoints available. Just go in your browser to endpoint url, and GUI will appear.

SETUP:

    Python3 and internet connection required!

    1 clone repo

    2 create virtualenv

    3 activate env

    4 pip install -r requirements.txt

    5 install and run postgresql(https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04)

    6 install redis(https://redis.io/topics/quickstart)

    7 create database and psql user properly: https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04

        dbname: "rss", username: "postgres", password "postgres",  host: localhost

    8 run migrations

    9 in tew terminal with env on, and in /src directory run: celery -A rss_scraper beat (scheduler for scheduling tasks)

    10 in tew terminal with env on, and in /src directory run: celery -A rss_scraper worker (worker for fetching data)

    11 run server


USAGE:

    I) GET /api/get_rates/ endpoint returns all Exchange rates in database, filtering and ordering is also possible.

          Response Example:

          {
            "base_currency": "EUR",
            "exchange_currency": "HUF",
            "rate": "323.4300",
            "fetched": "2018-08-21"
          },

          Ordering:

            <fields - order>

            1) exchange_currency - ascending
            2) exchange_currency - descending
            3) rate - ascending
            4) rate - descending
            5) fetched - ascending
            6) fetched - descending

                example request:
                    /api/get_rates/?ordering=-exchange_currency

          Filtering:

            <fields - type of filter input parameter>

            1) exchange_currency - string
            2) rate - decimal (8 digit max, 4 decimal places)
            2) fetched - date (YYYY-MM-DD)

                example request:
                    /api/get_rates/?exchange_currency=HUF&rate=324.3000&fetched=2018-08-24



    II) POST /api/add_source/ endpoint saves feed source to database

        required parameter: url (only from: https://www.ecb.europa.eu/home/html/rss.en.html)

        example request body: {"url":"https://www.ecb.europa.eu/rss/fxref-czk.html"}


TESTING:
    There are some automatic test written, to run them type: ./manage.py test
    also running celery worker is required to pass all tests.
    < in tew terminal with env on, and in /src directory run: celery -A rss_scraper worker (worker for fetching data)>

    For more details check functions docs in file /src/api/tests.py