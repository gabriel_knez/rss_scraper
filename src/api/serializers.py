from rest_framework import serializers
from .models import Feed, Rate


class FeedSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feed
        fields = ('url',)


class RateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rate
        fields = ('base_currency', 'exchange_currency', 'rate', 'fetched')
