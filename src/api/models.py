from django.db import models
from .validators import validate_feed_url


class Feed(models.Model):
    url = models.URLField(max_length=60, validators=[validate_feed_url])


class Rate(models.Model):
    base_currency = models.CharField(default="EUR", editable=False, max_length=3)
    exchange_currency = models.CharField(max_length=3)
    rate = models.DecimalField(max_digits=8, decimal_places=4)
    fetched = models.DateField()
