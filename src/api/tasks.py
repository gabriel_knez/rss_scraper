from __future__ import absolute_import, unicode_literals
from celery import shared_task
from .models import Feed, Rate
import grequests
from bs4 import BeautifulSoup as Soup
import datetime


@shared_task(name='fetch_rate')
def fetch_rate():
    url_list = Feed.objects.all().values_list('url', flat=True)
    rs = (grequests.get(u) for u in url_list)
    results = grequests.map(rs, exception_handler=exception_handler)
    results = [result for result in results if getattr(result, 'status_code', -1) == 200]
    for result in results:
        root = Soup(result.content, "xml")
        for a in root.find_all('item'):
            Rate.objects.get_or_create(
                fetched=datetime.datetime.strptime(a.date.text.split("T")[0], '%Y-%m-%d').date(),
                rate=a.statistics.value.text,
                exchange_currency=a.statistics.targetCurrency.text
            )


def exception_handler(request, exception):
    print("Request failed!")


