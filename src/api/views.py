from rest_framework.generics import CreateAPIView, ListAPIView
from .models import Feed, Rate
from .serializers import FeedSerializer, RateSerializer
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend


class CreateFeedView(CreateAPIView):
    queryset = Feed.objects.all()
    serializer_class = FeedSerializer


class GetRatesView(ListAPIView):
    queryset = Rate.objects.all()
    serializer_class = RateSerializer
    filter_backends = (OrderingFilter, DjangoFilterBackend)
    ordering_fields = ('exchange_currency', 'rate', 'fetched')
    filter_fields = ('exchange_currency', 'rate', 'fetched')
    ordering = ('exchange_currency',)
