from django.core.exceptions import ValidationError
import datetime


def validate_feed_url(value):
    if 'https://www.ecb.europa.eu/rss/' not in value:
        raise ValidationError("Only European Central Bank Urls are accepted!")

