from django.urls import path
from .views import CreateFeedView, GetRatesView

app_name = "api"

urlpatterns = [
    path('add_source/', CreateFeedView.as_view(), name="add_source"),
    path('get_rates/', GetRatesView.as_view(), name="get_rates"),
]
