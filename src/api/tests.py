from django.test import TestCase
from django.urls import reverse
from .models import Feed, Rate
from .tasks import fetch_rate


class RatesFeedsViewTests(TestCase):

    def test_no_required_parameter(self):
        """
        Test if response code is 400, when no required parameter has been given.
        """
        url = reverse('api:add_source')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 400)

    def test_basic_create_feed(self):
        """
        Test if api saves Feed.
        """
        url = reverse('api:add_source')
        self.client.post(url, {'url': 'https://www.ecb.europa.eu/rss/fxref-pln.html'})
        count = Feed.objects.all().count()
        self.assertNotEqual(count, 0)

    def test_basic_fetch_rates(self):
        """
        Test if celery saves rates.
        IMPORTANT: celery worker needs to be running
        """
        url = reverse('api:add_source')
        self.client.post(url, {'url': 'https://www.ecb.europa.eu/rss/fxref-pln.html'})
        count = Feed.objects.all().count()
        self.assertNotEqual(count, 0)
        fetch_rate.apply()
        count = Rate.objects.all().count()
        self.assertNotEqual(count, 0)

    def test_basic_list_rates(self):
        """
        Test if API returns Rates.
        IMPORTANT: celery worker needs to be running
        """
        url = reverse('api:add_source')
        self.client.post(url, {'url': 'https://www.ecb.europa.eu/rss/fxref-pln.html'})
        count = Feed.objects.all().count()
        self.assertNotEqual(count, 0)
        fetch_rate.apply()
        url = reverse('api:get_rates')
        response = self.client.get(url)
        self.assertNotEqual(len(response.json()), 0)
